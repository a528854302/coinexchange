package com.xxll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxll.domain.SysUserRole;

public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
}