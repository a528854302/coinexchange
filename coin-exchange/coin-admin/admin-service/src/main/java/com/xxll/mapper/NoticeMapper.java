package com.xxll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxll.domain.Notice;

public interface NoticeMapper extends BaseMapper<Notice> {
}