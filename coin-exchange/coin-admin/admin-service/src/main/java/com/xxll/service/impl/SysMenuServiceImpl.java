package com.xxll.service.impl;

import com.xxll.service.SysMenuService;
import com.xxll.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxll.domain.SysMenu;
import com.xxll.mapper.SysMenuMapper;

import java.util.List;

@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysMenuMapper sysMenuMapper ;

    /**
     * <h2>通过用户的id 查询用户的菜单数据</h2>
     * @param userId
     **/
    @Override
    public List<SysMenu> getMemusByUserId(Long userId) {
        // 1、如果该用户是超级管理员->>拥有所有的菜单
        if (sysRoleService.isSuperAdmin(userId)) {
            return list();
        }
        // 2、如果该用户不是超级管理员->>查询角色->查询菜单
        return sysMenuMapper.selectMenusByUserId(userId);
    }
}
